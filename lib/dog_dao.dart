import 'package:sqflite/sqflite.dart';

import 'database_provider.dart';
import 'dog.dart';

class DogDao {
  //Insert
  static Future<void> insertDog(Dog dog) async {
    final db = await DatabaseProvider.database;
    await db.insert('dogs', dog.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  //Select
  static Future<List<Dog>> dogs() async {
    final db = await DatabaseProvider.database;
    return Dog.toList(await db.query(
      'dogs',
    ));
  }

  //Update
  static Future<void> updateDog(Dog dog) async {
    final db = await DatabaseProvider.database;
    await db.update(
      'dogs',
      dog.toMap(),
      where: 'id = ?',
      whereArgs: [dog.id],
    );
  }

  //Delete
  static Future<void> deleteDog(int id) async {
    final db = await DatabaseProvider.database;
    await db.delete('dogs', where: 'id = ?', whereArgs: [id]);
  }
}
