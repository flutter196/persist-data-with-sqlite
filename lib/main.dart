import 'package:persist_data_with_sqlite/dog_dao.dart';
import 'dog.dart';

void main() async {
  var lily = Dog(id: 0, name: 'Lily', age: 5);
  var bluebell = Dog(id: 1, name: 'Bluebell', age: 2);

  //Insert
  await DogDao.insertDog(lily);
  await DogDao.insertDog(bluebell);

  print(await DogDao.dogs());

  //Update
  lily = Dog(id: lily.id, name: lily.name, age: lily.age + 2);
  bluebell = Dog(id: bluebell.id, name: bluebell.name, age: bluebell.age + 1);

  await DogDao.updateDog(lily);
  await DogDao.updateDog(bluebell);

  print(await DogDao.dogs());

  //Delete
  await DogDao.deleteDog(1);

  print(await DogDao.dogs());
}
